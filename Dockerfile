# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from gRPC package
FROM golang:latest

# Add Maintainer Info
LABEL maintainer="Mark Huang <admin@zh-code.com>"

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the source from the current directory to the Working Directory inside the container
COPY main .

# Command to run the executable
CMD ["./main"]
