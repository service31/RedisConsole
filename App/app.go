package main

import (
	"time"

	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entityBase "gitlab.com/service31/Data/Entity"
	addressEntity "gitlab.com/service31/Data/Entity/Address"
	businessEntity "gitlab.com/service31/Data/Entity/Business"
	orderEntity "gitlab.com/service31/Data/Entity/Order"
	productEntity "gitlab.com/service31/Data/Entity/Product"
	questEntity "gitlab.com/service31/Data/Entity/Quest"
	shoppingCartEntity "gitlab.com/service31/Data/Entity/ShoppingCart"
	userEntity "gitlab.com/service31/Data/Entity/User"
	module "gitlab.com/service31/Data/Module/Redis"
)

func main() {
	common.Bootstrap()
	sub := redisClient.RedisClient.Subscribe("ItemChange")
	for {
		msg, _ := sub.ReceiveMessage()
		go func() {
			message := &module.MessageItem{}
			err := message.Unmarshal([]byte(msg.Payload))
			logger.CheckError(err)
			if message.GetProperty() == redisClient.RedisChangeKeys.UserChange {
				userChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.ServiceChange {
				serviceChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.AddressChange {
				addressChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.ProductChange {
				productChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.CategoryChange {
				categoryChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.BusinessChange {
				businessChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.OrderChange {
				orderChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.QuestChange {
				questChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.QuestProgressChange {
				questProgressChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.PointsChange {
				pointsChange(message.GetID())
			} else if message.GetProperty() == redisClient.RedisChangeKeys.PromotionCodeChange {
				promotionCodeChange(message.GetID())
			}
		}()
	}
}

func userChange(id string) {
	uid, err := uuid.FromString(id)
	if logger.CheckError(err) || uid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetUserKey(id)
	user := &userEntity.User{}
	if db := common.DB.Where(userEntity.User{
		Base: entityBase.Base{
			ID: uid,
		},
	}).First(&user); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	serviceMapKey := redisClient.RedisMapKeys.GetUserByServiceMapKey(user.ServiceID.String(), user.ID.String())
	emailMapKey := ""
	usernameMapKey := ""
	phoneMapKey := ""

	if user.Email != "" {
		emailMapKey = redisClient.RedisMapKeys.GetUserStringPropertyMapKey(user.ServiceID.String(), user.Email)
	}
	if user.Username != "" {
		usernameMapKey = redisClient.RedisMapKeys.GetUserStringPropertyMapKey(user.ServiceID.String(), user.Username)
	}
	if user.PhoneNumber != "" {
		phoneMapKey = redisClient.RedisMapKeys.GetUserStringPropertyMapKey(user.ServiceID.String(), user.PhoneNumber)
	}
	if !user.IsActive {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.Del(serviceMapKey)
		if emailMapKey != "" {
			redisClient.RedisClient.HDel(emailMapKey)
		}
		if usernameMapKey != "" {
			redisClient.RedisClient.HDel(usernameMapKey)
		}
		if phoneMapKey != "" {
			redisClient.RedisClient.HDel(phoneMapKey)
		}
	} else {
		bytes, err := user.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.UserHashMap, serviceMapKey, key)
		if emailMapKey != "" {
			redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.UserHashMap, emailMapKey, key)
		}
		if usernameMapKey != "" {
			redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.UserHashMap, usernameMapKey, key)
		}
		if phoneMapKey != "" {
			redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.UserHashMap, phoneMapKey, key)
		}
	}
}

func serviceChange(id string) {
	sid, err := uuid.FromString(id)
	if logger.CheckError(err) || sid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetServiceKey(id)
	service := &userEntity.Service{}
	if db := common.DB.Where(userEntity.User{
		Base: entityBase.Base{
			ID: sid,
		},
	}).First(&service); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	usernameMapKey := redisClient.RedisMapKeys.GetServiceMapKey(service.Username)

	if !service.IsActive {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(redisClient.RedisMapKeys.ServiceUsernameHashMap, usernameMapKey)
	} else {
		bytes, err := service.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ServiceUsernameHashMap, usernameMapKey, key)
	}
}

func addressChange(id string) {
	aid, err := uuid.FromString(id)
	if logger.CheckError(err) || aid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetAddressKey(id)
	address := &addressEntity.Address{}
	if db := common.DB.Where(addressEntity.Address{
		Base: entityBase.Base{
			ID: aid,
		},
	}).First(&address); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	addressMapKey := redisClient.RedisMapKeys.GetAddressMapKey(address.EntityID.String(), address.ID.String())

	if !address.IsActive {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(addressMapKey)
	} else {
		bytes, err := address.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.AddressHashMap, addressMapKey, key)
		if address.IsDefault {
			redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.AddressHashMap, redisClient.RedisMapKeys.GetDefaultAddressMapKey(address.EntityID.String()), key)
		}
	}
}

func productChange(id string) {
	pid, err := uuid.FromString(id)
	if logger.CheckError(err) || pid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetProductKey(id)
	product := &productEntity.Product{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(productEntity.Product{
		Base: entityBase.Base{
			ID: pid,
		},
	}).First(&product); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	productMapKey := redisClient.RedisMapKeys.GetProductMapKey(product.EntityID.String(), product.ID.String())

	if !product.IsActive {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(productMapKey)
	} else {
		bytes, err := product.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ProductHashMap, productMapKey, key)
	}
}

func categoryChange(id string) {
	cid, err := uuid.FromString(id)
	if logger.CheckError(err) || cid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetCategoryKey(id)
	category := &productEntity.Category{}
	var updateValueMap map[string][]byte
	var updateMap map[string]string
	if db := common.DB.Set("gorm:auto_preload", true).Where(productEntity.Category{
		Base: entityBase.Base{
			ID: cid,
		},
	}).First(&category); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	tempID := category.ID.String()
	tempBytes, err := category.MarshalBinary()
	if logger.CheckError(err) {
		return
	}
	updateMap[redisClient.RedisMapKeys.GetCategoryMapKey(category.EntityID.String(), tempID)] = redisClient.RedisMapKeys.GetCategoryKey(tempID)
	updateValueMap[redisClient.RedisMapKeys.GetCategoryKey(tempID)] = tempBytes

	up := *category
	//up
	for up.Parent != nil {
		tempID = up.Parent.ID.String()
		tempBytes, err = up.MarshalBinary()
		if logger.CheckError(err) {
			continue
		}
		updateMap[redisClient.RedisMapKeys.GetCategoryMapKey(up.Parent.EntityID.String(), tempID)] = redisClient.RedisMapKeys.GetCategoryKey(tempID)
		updateValueMap[redisClient.RedisMapKeys.GetCategoryKey(tempID)] = tempBytes
		up = *up.Parent
	}

	down := *category
	//down
	for down.Child != nil {
		tempID = down.Parent.ID.String()
		tempBytes, err = up.MarshalBinary()
		if logger.CheckError(err) {
			continue
		}
		updateMap[redisClient.RedisMapKeys.GetCategoryMapKey(down.Child.EntityID.String(), tempID)] = redisClient.RedisMapKeys.GetCategoryKey(tempID)
		updateValueMap[redisClient.RedisMapKeys.GetCategoryKey(tempID)] = tempBytes
		down = *down.Parent
	}

	redisClient.SetRedisValues(updateValueMap)
	redisClient.SetRedisHashMapValues(redisClient.RedisMapKeys.CategoryHashMap, updateMap)
}

func businessChange(id string) {
	bid, err := uuid.FromString(id)
	if logger.CheckError(err) || bid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetBusinessKey(id)
	business := &businessEntity.Business{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(businessEntity.Business{
		Base: entityBase.Base{
			ID: bid,
		},
	}).First(&business); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	businessMapKey := redisClient.RedisMapKeys.GetBusinessMapKey(business.EntityID.String(), business.ID.String())
	businessUserMapKey := redisClient.RedisMapKeys.GetBusinessMapKey(business.AdminUserID.String(), business.ID.String())

	if !business.IsActive {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(businessMapKey)
		redisClient.RedisClient.HDel(businessUserMapKey)
	} else {
		bytes, err := business.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.BusinessHashMap, businessMapKey, key)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.BusinessHashMap, businessUserMapKey, key)
	}
}

func orderChange(id string) {
	oid, err := uuid.FromString(id)
	if logger.CheckError(err) || oid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetOrderKey(id)
	order := &orderEntity.Order{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(orderEntity.Order{
		Base: entityBase.Base{
			ID: oid,
		},
	}).First(&order); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	bytes, err := order.MarshalBinary()
	if logger.CheckError(err) {
		return
	}
	redisClient.SetRedisValue(key, bytes)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.OrderHashMap, redisClient.RedisMapKeys.GetOrderMapKey(order.EntityID.String(), order.ID.String()), key)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.OrderHashMap, redisClient.RedisMapKeys.GetOrderMapKey(order.BusinessInfo.BusinessID.String(), order.ID.String()), key)
}

func questChange(id string) {
	qid, err := uuid.FromString(id)
	if logger.CheckError(err) || qid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetQuestKey(id)
	quest := &questEntity.Quest{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(questEntity.Quest{
		Base: entityBase.Base{
			ID: qid,
		},
	}).First(&quest); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	questMapKey := redisClient.RedisMapKeys.GetQuestMapKey(quest.EntityID.String(), quest.ID.String())

	if !quest.IsActive {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(questMapKey)
	} else {
		bytes, err := quest.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.QuestHashMap, questMapKey, key)
	}
}

func questProgressChange(id string) {
	qid, err := uuid.FromString(id)
	if logger.CheckError(err) || qid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetQuestProgressKey(id)
	questProgress := &questEntity.QuestProgress{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(questEntity.QuestProgress{
		Base: entityBase.Base{
			ID: qid,
		},
	}).First(&questProgress); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	questProgressMapKey := redisClient.RedisMapKeys.GetQuestProgressMapKey(questProgress.EntityID.String(), questProgress.ID.String())
	progressOfQuestMapKey := redisClient.RedisMapKeys.GetProgressOfQuestMapKey(questProgress.AcceptedQuest.ID.String(), questProgress.ID.String())

	if !questProgress.AcceptedQuest.IsActive ||
		questProgress.IsCancelled || questProgress.IsCompleted {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(questProgressMapKey)
		redisClient.RedisClient.HDel(progressOfQuestMapKey)
	} else {
		bytes, err := questProgress.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.QuestProgressHashMap, questProgressMapKey, key)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.QuestProgressHashMap, progressOfQuestMapKey, questProgress.AcceptedQuest.ID.String())
	}
}

//pointsChange it's entityID based
func pointsChange(entityID string) {
	pid, err := uuid.FromString(entityID)
	if logger.CheckError(err) || pid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetPointsKey(entityID)
	points := &questEntity.Points{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(questEntity.Points{
		Base: entityBase.Base{
			ID: pid,
		},
	}).First(&points); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	bytes, err := points.MarshalBinary()
	if logger.CheckError(err) {
		return
	}
	redisClient.SetRedisValue(key, bytes)
}

func promotionCodeChange(id string) {
	pid, err := uuid.FromString(id)
	if logger.CheckError(err) || pid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetPromotionCodeKey(id)
	promotionCode := &shoppingCartEntity.PromotionCode{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(shoppingCartEntity.PromotionCode{
		Base: entityBase.Base{
			ID: pid,
		},
	}).First(&promotionCode); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	promotionCodeMapKey := redisClient.RedisMapKeys.GetPromotionCodeMapKey(promotionCode.EntityID.String(), id)
	promotionCodeCodeMapKey := redisClient.RedisMapKeys.GetPromotionCodeByPromotionCodeMapKey(promotionCode.EntityID.String(), promotionCode.Code)

	if !promotionCode.IsActive ||
		time.Now().Sub(promotionCode.ActiveFrom).Minutes() < 0 || time.Now().Sub(promotionCode.ActiveTo).Minutes() > 0 {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(promotionCodeMapKey)
		redisClient.RedisClient.HDel(promotionCodeCodeMapKey)
	} else {
		bytes, err := promotionCode.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.PromotionCodeHashMap, promotionCodeMapKey, key)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.PromotionCodeHashMap, promotionCodeCodeMapKey, key)
	}
}

func shoppingCartChange(id string) {
	sid, err := uuid.FromString(id)
	if logger.CheckError(err) || sid == uuid.Nil {
		return
	}
	key := redisClient.RedisMapKeys.GetShoppingCartKey(id)
	shoppingCart := &shoppingCartEntity.ShoppingCart{}
	if db := common.DB.Set("gorm:auto_preload", true).Where(shoppingCartEntity.ShoppingCart{
		Base: entityBase.Base{
			ID: sid,
		},
	}).First(&shoppingCart); db.RecordNotFound() {
		redisClient.RedisClient.Del(key)
		return
	} else if logger.CheckError(db.Error) {
		return
	}

	shoppingCartBusinessMapKey := redisClient.RedisMapKeys.GetShoppingCartMapKey(shoppingCart.BusinessID.String(), id)
	shoppingCartUserMapKey := redisClient.RedisMapKeys.GetShoppingCartMapKey(shoppingCart.EntityID.String(), id)
	shoppingCartByEntityAndBusinessMapKey := redisClient.RedisMapKeys.GetShoppingCartsBySpecificEntityAndBusinessMapKey(shoppingCart.EntityID.String(), shoppingCart.BusinessID.String())

	if !shoppingCart.IsActive || shoppingCart.IsCompleted {
		redisClient.RedisClient.Del(key)
		redisClient.RedisClient.HDel(shoppingCartBusinessMapKey)
		redisClient.RedisClient.HDel(shoppingCartUserMapKey)
		redisClient.RedisClient.HDel(shoppingCartByEntityAndBusinessMapKey)
	} else {
		bytes, err := shoppingCart.MarshalBinary()
		if logger.CheckError(err) {
			return
		}
		redisClient.SetRedisValue(key, bytes)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ShoppingCartHashMap, shoppingCartBusinessMapKey, key)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ShoppingCartHashMap, shoppingCartUserMapKey, key)
		redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ShoppingCartHashMap, shoppingCartByEntityAndBusinessMapKey, key)
	}
}
