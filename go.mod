module gitlab.com/service31/RedisConsole

go 1.13

require (
	github.com/grpc-ecosystem/grpc-gateway v1.14.3 // indirect
	github.com/satori/go.uuid v1.2.0
	gitlab.com/service31/Common v0.0.0-20200405201433-75af8a8de7e7
	gitlab.com/service31/Data v0.0.0-20200405201411-6bb61c425b5b
)
